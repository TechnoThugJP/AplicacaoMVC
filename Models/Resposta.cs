using System.ComponentModel.DataAnnotations;
namespace ProjetoEnquete.Models
{
    public class Resposta
    {
        [Required(ErrorMessage ="O Campo Nome deve ser Preenchido.")]
        public string Nome { get; set; }
        [Required(ErrorMessage ="O Campo Email deve ser Preenchido.")]
        [EmailAddress(ErrorMessage ="O campo E-Mail não corresponde a um endereço Valido")]
        public string Email { get; set; }
        [Required(ErrorMessage ="O Campo Resposta deve ser Preenchido.")]
        public bool? Sim { get; set; }
    }
}