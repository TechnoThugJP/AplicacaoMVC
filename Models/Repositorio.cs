namespace ProjetoEnquete.Models
{
    public static class Repositorio
    {
        private static List<Resposta> respostas = new List<Resposta>();
        public static IEnumerable<Resposta> Respostas {get {return respostas; } }
        public static void AdicionarResposta(Resposta resposta)
        {
            respostas.Add(resposta);
        }

        static Repositorio()
        {
            respostas.Add(new Resposta()
            { Nome = "Gabriel", Email = "GAbriel@hotmail.com", Sim = true});
              respostas.Add(new Resposta()
            { Nome = "Fernando", Email = "Fernando@hotmail.com", Sim = false});
              respostas.Add(new Resposta()
            { Nome = "Dos", Email = "Dos@hotmail.com", Sim = false});
              respostas.Add(new Resposta()
            { Nome = "Santos", Email = "Santos@hotmail.com", Sim = true});
              respostas.Add(new Resposta()
            { Nome = "Guerra", Email = "Guerra@hotmail.com", Sim = true});
        }
    }
}